#I Love Vivas SO MUCH
This is a simple JS script that exclaims one's love of Viva puffs to the world. 
To use this file, simply run the JS file from the command line. 

##License
----------------------------------------------------------------------------
"THE BEER-WARE LICENSE":
<ameinzinger0917@conestogac.on.ca> wrote this file.  As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return.   - Alex Meinzinger
----------------------------------------------------------------------------
 I chose the beer-ware license because it's very similar to the MIT license,
 however it is simpler in it's implementation and encourages rewarding the 
 creater with a  beer. :)
 (https://fedoraproject.org/wiki/Licensing/Beerware)